#file-name: dockerfile
FROM ubuntu:latest

# Run the Update
RUN apt-get update && apt-get upgrade -y

# Install pre-reqs
# RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get install -y curl openssh-server python3.8 python3-pip --no-install-recommends apt-utils

# Setup sshd
RUN mkdir -p /var/run/sshd
RUN echo 'root:password' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# download and install pip
RUN curl -sO https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py

# install AWS CLI
RUN pip3 install awscli

# Setup AWS CLI Command Completion
RUN echo complete -C '/usr/local/bin/aws_completer' aws >> ~/.bashrc
CMD /usr/sbin/sshd -D

EXPOSE 22

#=========POSTGRES========#
#RUN apt update
#RUN apt -y install postgresql postgresql-client
#RUN apt -y install postgresql-client
#RUN service postgresql start

#EXPOSE 5432
#CMD ["postgres"]

#Make sure that your shell script file is in the same folder as your dockerfile while running the docker build command as the below command will copy the file to the /home/root/ folder for execution. 
COPY . /home/root/ 
#Copying script file
USER root 
#switching the user to give elevated access to the commands being executed from the k8s cron job
